/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.marcovy.webapp;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author MarCovy
 */
public class Login extends HttpServlet{
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Cookie[] cookies = request.getCookies();
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            String form = "<form method=\"POST\" action=\"loginprocess\">"
                    + "<input type=\"text\" name=\"login\" placeholder=\"Login\"/> "
                + "<input type=\"text\" name=\"pass\" placeholder=\"Haslo\"/>"
                    + "<input type=\"submit\" value=\"Loguj\"/></form>";
            HttpSession session = request.getSession(false);
            String user = null;
            if (session != null && session.getAttribute("user") != null && session.getAttribute("user").equals("vagrant")) {
                form = "ALREADY LOG-IN!"
                                + "<br>"
                                + "<a href=\"logout\" >Wyloguj</a>";
            }

            out.println(form);
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
